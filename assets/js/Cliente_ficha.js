function ficha() {
    var nombre = $('#nombre').val();
    var apellido = $('#apellido').val();
    var ci = $('#CI').val();
    var genero = $('#Genero :checked').val();
    var ruc = $('#RUC').val();
    var telefono = $('#telefono').val();
    var telefono1 = $('#telefono1').val();
    var direcion = $('#direccion').val();
    var calle1 = $('#calle1').val();
    var calle2 = $('#calle2').val();
    var nroCasa = $('#nroCasa').val();
    var ciudad = $('#ciudad').val();
    var latitud = $('#latitud').val();
    var longitud = $('#longitud').val();

    if (nombre == "" || ci == "" || telefono == "") {
        alert("campos obligatorios son Nombre, CI, Telefono");
    } else {
        $.ajax({
            url: localStorage.url + 'cliente/NewFicha',
            type: 'post',
            data: {Vnombre: nombre, Vapellido: apellido, Vci: ci, Vgenero: genero, Vruc: ruc, Vtelefono: telefono, Vtelefono1: telefono1, Vdireccion: direcion, Vcalle1: calle1, Vcalle2: calle2, VnroC: nroCasa, Vciudad: ciudad, Vlactitud: latitud, Vlongitud: longitud},
            success: function (data, textStatus, jqXHR) {
                var DataR = JSON.parse(data);
                if (DataR.ficha) {
                    $("#central").empty();
                    $("#central").html(DataR.ficha);
                    $.notify(DataR.info, "success");
                } else {
                    $.notify(DataR.info, "warn");
                    $(":text").val('');
                    $(":radio").val('checked="false"');
                    $(":selected").val('');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(' Error server ' + textStatus);
            }
        });
    }
}

function ViewsCliente() {
    $.ajax({
        url: localStorage.url + 'cliente/ViewFicha',
        success: function (data, textStatus, jqXHR) {
            var DataR = JSON.parse(data);
            $("#central").empty();
            $("#central").html(DataR.ficha);
            // $.notify(DataR.info, "success");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(' Error server ' + textStatus);
        }
    });
}

function filtroCI() {
    var Ci = $('#seardh').val();
    var tipo = 'Ci';
    $.ajax({
        url: localStorage.url + 'cliente/filtroClient',
        type: 'post',
        data: {VIdent: Ci, Vtipo: tipo},
        success: function (data, textStatus, jqXHR) {
            var DataR = JSON.parse(data);
            $("#central").empty();
            $("#central").html(DataR.ficha);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(' Error server ' + textStatus);
        }
    });
}

function filtroTelef() {
    var tele = $('#seardh').val();
    var tipo = 'tel';
    $.ajax({
        url: localStorage.url + 'cliente/filtroClient',
        type: 'post',
        data: {VIdent: tele, Vtipo: tipo},
        success: function (data, textStatus, jqXHR) {
            var DataR = JSON.parse(data);
            $("#central").empty();
            $("#central").html(DataR.ficha);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(' Error server ' + textStatus);
        }
    });
}

function IrPago(idCli, idDir) {
    var nombre = $('#nombre').val();
    var apellido = $('#apellido').val();
    var ci = $('#CI').val();
    var genero = $('input[name="optradio"]:checked').val();
    var ruc = $('#RUC').val();
    var telefono = $('#telefono').val();
    var telefono1 = $('#telefono1').val();
    var direcion = $('#direccion').val();
    var calle1 = $('#calle1').val();
    var calle2 = $('#calle2').val();
    var nroCasa = $('#nroCasa').val();
    var ciudad = $('#ciudad').val();
    var latitud = $('#latitud').val();
    var longitud = $('#longitud').val();

    if (nombre == "" || ci == "" || telefono == "") {
        alert("campos obligatorios son Nombre, CI, Telefono");
    } else {
        $.ajax({
            url: localStorage.url + 'cliente/UptaFicha',
            type: 'post',
            data: {Vnombre: nombre, Vapellido: apellido, Vci: ci, Vgenero: genero, Vruc: ruc, Vtelefono: telefono, Vtelefono1: telefono1, Vdireccion: direcion, Vcalle1: calle1, Vcalle2: calle2, VnroC: nroCasa, Vciudad: ciudad, Vlactitud: latitud, Vlongitud: longitud, VidClie: idCli, VidDir: idDir},
            success: function (data, textStatus, jqXHR) {
                var DataR = JSON.parse(data);
                if (DataR.Paga) {
                    $("#central").empty();
                    $("#central").html(DataR.Paga);
                    $.notify(DataR.info, "success");
                } else {
                    $.notify(DataR.info, "warn");
                    $(":text").val('');
                    $(":radio").val('checked="false"');
                    $(":selected").val('');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(' Error server ' + textStatus);
            }
        });
    }
}


function newPed(){
     var tipoPago= $('input[name="pago"]:checked').val();
     var  montoPaga  = $('input[name="Monto"]').val();
     
     $.ajax({
         url:localStorage.url + 'pedido/newPedido',
         type: 'post',
         data:{VpagaCon: montoPaga ,VtiPago: tipoPago},
         success: function (data, textStatus, jqXHR) {
            var dataR =  JSON.parse(data);
             if(dataR.info){
                 load('1');
                 $.notify(dataR.info ,"success")
             }else{
                 $.notify(dataR.infoError ,"warn")
             }
            
        }
         
     })
}