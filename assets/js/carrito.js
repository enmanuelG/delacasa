$(document).ready(function(){
    $("#id").click(function(){
        get_Productos('-1');
    });
    
});

function addcarrito(){
    var variableFor = $('#formsimple').serializeArray();
    var qt = variableFor[0].value;
    var ipr =  variableFor[1].value;
    var Des = variableFor[2].value;
    var tipo = $('#formsimple').data('tipo');
    
    if(tipo === 2){
        var checks = ValidaSabor();
        var maxVar = $('#formsimple').data('tipo');
        if(checks.length <= maxVar && checks.length != 0){
             $.ajax({
                url:localStorage.url+'carrito/addCarrito',
                type:'post',
                data:{idProducto: ipr,qt: qt,descripcion: Des, tipoP:tipo, Sabores: checks},
                success: function (data, textStatus, jqXHR) {
                       get_Productos(-1);
                       var DataR =   JSON.parse(data);
                       $("#Carrito").empty();
                       $("#Carrito").html(DataR.carrito);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                  console.log(' Error server '+textStatus);  
                }
            });
        }else{
            alert('solo puede elegir : '+maxVar+' items');
        }
    }else{
        $.ajax({
                url:localStorage.url+'carrito/addCarrito',
                type:'post',
                data:{idProducto: ipr,qt: qt,descripcion: Des, tipoP:tipo},
                success: function (data, textStatus, jqXHR) {
                       get_Productos(-1);
                       var DataR =   JSON.parse(data);
                       $("#Carrito").empty();
                       $("#Carrito").html(DataR.carrito);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                  console.log(' Error server '+textStatus);  
                }
        });
    }
    
}


function deleCarrit(idP, ProductoName){
    $.ajax({
            url:localStorage.url+'carrito/delItemCarrito',
            type:'post',
            data:{idPoD: idP, product:ProductoName},
            success: function (data, textStatus, jqXHR) {
                   var DataR =   JSON.parse(data);
                   $("#Carrito").empty();
                   $("#Carrito").html(DataR.carrito);
            },
            error: function (jqXHR, textStatus, errorThrown) {
              console.log(' Error server '+textStatus);  
            }
    });
}

function  ValidaSabor(){
        var checked = [];
        var cont = 0;
       $("input:checkbox[name=sabores]:checked").each(function() {
           checked[cont] = $(this).val();
           cont++;
        });
        
        return checked;
    }
    
 function limpiarCarrito(){
     $.ajax({
                url:localStorage.url+'carrito/limpiarCarrito',
                success: function (data, textStatus, jqXHR) {
                       get_Productos(-1);
                       var DataR =   JSON.parse(data);
                       $("#Carrito").empty();
                       $("#Carrito").html(DataR.carrito);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                  console.log(' Error server '+textStatus);  
                }
        });
    }  
    
   function tipoPedido(){
       var ped =$('input:checkbox[name=tipoPed]:checked').val();
       $.ajax({
                url:localStorage.url+'carrito/tipoPedido',
                type: 'post',
                data: {tipoPedido:ped},
                success: function (data, textStatus, jqXHR) {
                       var DataR =   JSON.parse(data);
                       $("#Carrito").empty();
                       $("#Carrito").html(DataR.carrito);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                  console.log(' Error server '+textStatus);  
                }
        });
   }     