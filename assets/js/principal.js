
$(document).ready(function(){
    $("ul:first li").click(function(){
      var module = $(this).val();
      load(module);
    });
    
    $("#CancelProducto").click(function(){
        get_Productos('-1')
    });
    
     $(".input-group-btn .dropdown-menu li a").click(function(){
        var selText = $(this).html();
       $(this).parents('.input-group-btn').find('.btn-search').html(selText);
   }); 
});

function load(idM){
   $.ajax({
       url: localStorage.url+'Load',
       type: 'Post',
       data: {module : idM},
        success: function (data, textStatus, jqXHR) {
          var DataR =   JSON.parse(data);
         $("#principal").empty();
         $("#principal").html(DataR.primario);
         $("#secundario").empty();
         $("#secundatio").html(DataR.secundario);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(' Error server '+textStatus);  
        } 
   });
}

function get_Productos(idcate){
    $.ajax({
        url:localStorage.url+'producto/getListProducto',
        type: 'Post',
        data:{idCategoria:idcate},
        success: function (data, textStatus, jqXHR) {
           if(data){
             var DataR = data;
            $("[name^='Menu']").attr('disabled', false);
            $("#lisProducto").empty();
            $("#lisProducto").html(DataR);
           } 
        }
    });
}

function Producto(idproducto,stock){
    
    if(stock > 0){
        $.ajax({
            url:localStorage.url+'producto/viewsProducto',
        type: 'Post',
        data:{idproducto:idproducto},
        success: function (data, textStatus, jqXHR) {
           if(data){
             var DataR = data;
            $("#cba").val('')
            $("[name^='Menu']").attr('disabled', true);
            $("#lisProducto").empty();
            $("#lisProducto").html(DataR);
           } 
        }
    });
    }else{
        $.notify("producto sin stock",'danger');
    }
}

function tickets($idpe){
    $.ajax({
        url:localStorage.url+'pedido/ViewsTickets',
        type: 'Post',
        data:{idPed:$idpe},
        success: function (data, textStatus, jqXHR) {
            var DataR =   JSON.parse(data);
           if(DataR.tickets){
               var ticket = DataR.tickets;
               var myWindow = window.open("", "myWindow", "width=640,height=800,fullscreen=no,resizable,scrollbars");
               myWindow.document.write(ticket);
           }else{
               var myWindow = window.open("", "", "width=200,height=23");
               myWindow.document.write("No hay datos que mostrar");
           } 
        }
    }); 
}

//function Report(){
//    var Vfecha1 = $('#fechaI').val();
//    var Vfecha2 = $('#fechaH').val();
//    var VTipo = $('#tipo').val();
//    $.ajax({
//        url:localStorage.url+'reporteria/getReport',
//        type: 'Post',
//        data:{fechaIn:Vfecha1, fechaFin: Vfecha2,Tipo: VTipo}
//    });
//}

function listPedEstados(idPed, idEstado){
   var  VidEstado = idEstado;
     $.ajax({
        url:localStorage.url+'pedido/updateEstado',
        type: 'Post',
        data:{idEstado:VidEstado, idPedido:idPed},
        success: function (data, textStatus, jqXHR) {
            var DataR =   JSON.parse(data);
           if(DataR.listPedido){
               $('#principal').empty();
               $('#principal').html(DataR.listPedido);
                $.notify(DataR.info, "success");
           }
        }
    });
}

function get_ProductoCategoria(idcate){
    $.ajax({
        url:localStorage.url+'producto/getProdCate',
        type: 'Post',
        data:{idCategoria:idcate},
        success: function (data, textStatus, jqXHR) {
            var DataR = JSON.parse(data);
           if(DataR.html){
            $("#tabaConten").empty();
            $("#tabaConten").html(DataR.html);
            $("#tabaConten").scrollTop(260);
           } 
        }
    });
}

function updateProducto(idPro){
    // var formulario = $("prueba_"+idPro+"").serialize();
       var code = $('input[name="code_'+idPro+'"]').val();
       var produc = $('input[name="producto_'+idPro+'"]').val();
       var abre = $('input[name="abre_'+idPro+'"]').val();
       var precio = $('input[name="precio_'+idPro+'"]').val();
       var stock = $('input[name="stock_'+idPro+'"]').val();
       var estado = $('input[name="radio_'+idPro+'"]:checked').val();
       var Descripcion = $('#coment_'+idPro+'').val();
       $.ajax({
          url: localStorage.url+'producto/updatePro',
          type:'Post',
          data:{cod: code, Prod: produc, abr:abre, prec:precio, stock: stock, est: estado, descrip: Descripcion,  idProd: idPro},
          success: function (data, textStatus, jqXHR) {
            var dataR = JSON.parse(data);
            if(dataR.categoriaID){
                get_ProductoCategoria(dataR.categoriaID);
                $.notify( dataR.msn, 'success');
            }
        } 
       });
   }
   
   function DeleteProd(idPro){
       $.ajax({
          url: localStorage.url+'producto/deletProducto',
          type:'Post',
          data:{idProd: idPro},
          success: function (data, textStatus, jqXHR) {
            var dataR = JSON.parse(data);
            if(dataR.categoriaID){
                get_ProductoCategoria(dataR.categoriaID);
                $.notify( dataR.msn, 'danger');
            }
        } 
       });
   }
   
   function ViewsReproducto(){
       $.ajax({
           url: localStorage.url+'producto/ViewsNewProduct',
           type: 'POST',
           success: function (data, textStatus, jqXHR) {
             var dataR = JSON.parse(data); 
             $('#newProduct').empty();
             $('#newProduct').html(dataR.producto);
           }
       });
   }
   
   function insProducto(){
       var formu = $('#Product').serializeArray()
       var validacion = validaProduct(formu);
       
       if(validacion == true){
          $.ajax({
          url: localStorage.url+'producto/insProducto',
          type: 'POST',
          data: {productos: formu},
                success: function (data, textStatus, jqXHR) {
                    var dataR = JSON.parse(data);
                     load(4); 
                     $.notify( dataR.msn, 'success');
                }
            });
       }else{
                alert('Complete con todos los campos necesarios');
            }
   }
   
   function validaProduct(producto){
       var list = producto;
       var i = 0;
       var conT = 0;
       for (i; i < list.length; i++ ){
           if(list[i].value === ''){
               $('input[name="'+list[i].name+'"]').css('border','2px solid red');
               conT++;
           }
       }
       return  conT > 0 ? false : true;
   }
   
   function validaSabor(){
      var tipo = $('#tipo').val();
      if(tipo === '3'){
        $('#pizza').prop('disabled', false);
      }else{
        $('#pizza').prop('disabled', true);
      }
   }
   