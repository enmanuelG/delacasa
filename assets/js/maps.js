var sucursal = new google.maps.LatLng(-25.337698, -57.565381);

function initMap() {
    var pval = new google.maps.LatLng(Number(document.getElementById("latitud").value), Number(document.getElementById("longitud").value));
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
    var myCenter = typeof pval === 'object' ? pval : new google.maps.LatLng(-25.329103, -57.553192);
    var mapa_lima = {
        //Muestra las coordenadas al centro del mapa
        center: sucursal,
        //Zoom del mapa 
        zoom: 7,
        // Tipo de mapa: ROADMAP, SATELLITE, HYBRID, TERRAIN 
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
// Creamos un mapa en el contenedor con id map_lima,  usando los parámetros de la variable mapa_lima
    var map = new google.maps.Map(document.getElementById("map"), mapa_lima);
//Mostramos el marcador en las coordenadas almacenada en la variable myCenter
    var marker = new google.maps.Marker({
        position: myCenter,
        draggable: true,
        map: map,
        title: "Your location"
    });

    var markerS = new google.maps.Marker({
        position: sucursal,
        draggable: false,
        map: map,
        title: "Your location",
        icon: localStorage.url + 'assets/imag/DeLaCAsaLogoMap.png'
    });


    google.maps.event.addListener(marker, 'click', function (overlay, point) {
        document.getElementById("latitud").value = this.getPosition().lat();
        document.getElementById("longitud").value = this.getPosition().lng();
        calculateAndDisplayRoute(directionsService, directionsDisplay);
        var point = new google.maps.LatLng(Number(document.getElementById("latitud").value), Number(document.getElementById("longitud").value));
        contentString = calculaDistanci(sucursal, point);
        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        infowindow.open(map, marker);
    });
//Añadimos el marcador para el mapa utilizando el método setMap()
    marker.setMap(map);
    directionsDisplay.setMap(map);
    calculateAndDisplayRoute(directionsService, directionsDisplay);
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    directionsService.route({
        origin: {lat: -25.337698, lng: -57.565381},
        destination: {lat: Number(document.getElementById("latitud").value), lng: Number(document.getElementById("longitud").value)},
        travelMode: 'DRIVING'
    }, function (response, status) {
        if (status === 'OK') {
            directionsDisplay.setDirections(response);
        } else {
            window.alert('tiene que mover el icon y darle clic para geo referenciarce.');
        }
    });
}

function calculaDistanci(marcadorA, marcadorB) {
    var distancia = google.maps.geometry.spherical.computeDistanceBetween(marcadorA, marcadorB);
    if(distancia > 1000){
        var resutl = (distancia /1000);
        return 'Distancia del local '+resutl.toFixed(1)+' km';
    }else{
        var resutl = distancia;
        return 'Distancia del local '+distancia.toFixed(0)+' metros';
    } 
}
