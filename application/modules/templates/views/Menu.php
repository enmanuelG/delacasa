<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row" style="margin-left: 15px; margin-right: 15px" id="altura">
    <div class="col-md-8 well" id="central" style="margin-left: 50px">
        <div class="panel panel-default">
            <div class="panel-body">  
                <div class="well well-lg" style="margin-left: 15px">
                    <div class="row">
                        <div class="col-md-3" style="margin: 0px; padding: 0px">
                            <div>
                                <img src="<?php echo base_url() . 'assets/imag/' . $local->imagen ?>" height="80" width="80" class="img-responsive">                            
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12 col-x text-center text-info">
                                    <h4><?php echo 'Local: ' . $local->local ?></h4>
                                </div>
                                <div class="col-md-12 text-center" > <?php echo 'Horarios: ' . $local->horario_ini . ' a ' . $local->horario_fin ?></div>
        <!--                        <div class="col-md-6"><?php // echo 'Horario Apertura: '.$local->horario_fin ?></div>-->
        <!--                        <p class="text text-center text-info"></p>-->
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
                <div class="col-md-3" style="margin: 0px">
                    <div class="panel panel-default" >
                        <div class="panel-heading text-center" style="padding: 0px"><h4>Menu</h4></div>
                        <div class="panel-body" style="padding:0px">
                            <div class="list-group" style="padding: 0px; margin: 0px">
                                <?php foreach ($menu as $list): ?>
                                    <button  onclick="get_Productos('<?php echo $list->idCategoria; ?>')" type="button" name="Menu" class="btn btn-primary btn-block" data-id="<?php echo $list->idCategoria; ?>" style="border-radius: 0px; margin: 1px"><p class="text-left"><?php echo $list->categoria; ?></p></button>
                           <!--             <a href="#" class="list-group-item"><?php echo $list->categoria; ?></a>-->
                                    <!--                  <div id="demo" class="collapse">
                                                              <button type="button" class="btn btn-info btn-block" data-toggle="collapse" data-target="#demo" style="border-radius: 0px;"><?php echo $list->categoria; ?></button>
                                                      </div>   -->
                                <?php endforeach; ?>
                            </div>    
                        </div>
                    </div> 
                </div>
                <div class="col-md-9" style="padding: 0px" id="lisProducto">
                    <div class="panel panel-primary" style="margin:1px">
                        <div class="panel-heading text text-center">listado de productos</div>
                        <div class="panel-body" style="padding: 0px;">
                            <a href="#" class="list-group-item">
                                 <p class="text text-danger">No hay productos para mostra</p>
                            </a>
                           
                        </div>
                    </div>   
                </div>
            </div>       
        </div>
    </div>    
    <div class="col-md-3 well" style="margin-left: 15px" id="Carrito">
        <?php echo Modules::run('carrito/iniCarrito'); ?>
    </div>
</div> 
