<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Template_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function get_menu(){
        $user = $user = $this->session->userData;
        $this->db->select('*');
        $this->db->from('categoria_producto');
        $this->db->where('idLocal', $user->idLocal);
        $this->db->where('estado','1');
        $resultado = $this->db->get();
        return $resultado->result();
    }
    
    
    public function get_dtosLocal(){
        $user = $user = $this->session->userData;
        $this->db->select('*');
        $this->db->from('det_local');
        $this->db->where('idLocal', $user->idLocal);
        $resultado = $this->db->get();
        
        return $resultado->row();
    }
    

}
