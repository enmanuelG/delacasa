<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends RH_Controller {
    
     public function __construct() {
        parent::__construct();
        $this->load->model('M_cliente');
    }
    public function ficha($data = null) {
        $this->load->view('cliente_ficha', $data);
    }
    
    public function NewFicha(){
        $ficha = $this->DataClientes();
        $valida = (int) $this->M_cliente->getClienteCant($ficha);
        if( $valida == 0){
            $idCliente = $this->M_cliente->InsCliente($ficha);
            $this->M_cliente->InsDireccion($ficha, $idCliente);
            $data['ficha'] = $this->M_cliente->getClienteID($idCliente);
            $data['direccion'] = $this->M_cliente->getDirToClienteID($idCliente);
            $View['info'] = 'Se registro el cliente';  
            $View['ficha'] = Modules::run('Cliente/ficha',$data);
            echo json_encode($View);
            } else {    
            $View['info'] = 'cliente ya existe';    
            echo json_encode($View);
            }
    }
    
    private function DataClientes(){
      $dataFicha = new stdClass;  
      $dataFicha->nombre = $this->input->post('Vnombre');
      $dataFicha->apell = $this->input->post('Vapellido');
      $dataFicha->CI = $this->input->post('Vci');
      $dataFicha->genero = $this->input->post('Vgenero');
      $dataFicha->ruc = $this->input->post('Vruc');
      $dataFicha->telefon = $this->input->post('Vtelefono');
      $dataFicha->telefon1 = $this->input->post('Vtelefono1');
      $dataFicha->Direccion = $this->input->post('Vdireccion');
      $dataFicha->calle1 = $this->input->post('Vcalle1');
      $dataFicha->calle2 = $this->input->post('Vcalle2');
      $dataFicha->nroCasa = $this->input->post('VnroC');
      $dataFicha->ciudad = $this->input->post('Vciudad');
      $dataFicha->latitud = $this->input->post('Vlactitud');
      $dataFicha->longitud = $this->input->post('Vlongitud');
      return $dataFicha;
    }
    
    public function ViewFicha(){
        $Views['ficha'] = Modules::run('Cliente/ficha');
        echo json_encode($Views);
    }
    
    public function filtroClient(){
        $tipo = $this->input->post('Vtipo');
        $Ident = $this->input->post('VIdent');
        
        switch ($tipo){
            case 'Ci':
                $data['ficha'] = isset($Ident) ? $this->M_cliente->getClienteToCi($Ident) : null;
                $data['direccion'] = isset($data['ficha']->idCliente) ? $this->M_cliente->getDirToClienteID($data['ficha']->idCliente) : '';  
            break;
            case 'tel':
                $Ident = $Ident != '' ? $Ident : null;
                $data['ficha'] = isset($Ident) ? $this->M_cliente->getClienteToCell($Ident): null; 
                $data['direccion'] = isset($data['ficha']->idCliente) ? $this->M_cliente->getDirToClienteID($data['ficha']->idCliente): ''; 
            break;
        }
        $View['ficha'] = Modules::run('Cliente/ficha',$data);
         echo json_encode($View);
    }
    
    public function UptaFicha(){
        $idCliente = $this->input->post('VidClie');
        $idDir = $this->input->post('VidDir');
        $fichaNew = $this->DataClientes();
        $this->M_cliente->updateCliente($fichaNew, $idCliente);
        $this->M_cliente->updateDir($fichaNew, $idDir);
        $this->session->set_userdata('tmpFicha',$fichaNew);
        $this->session->set_userdata('idCliente',$idCliente);
        $this->session->set_userdata('idDir',$idDir);
        
        $views['info'] = 'Se actualizo la ficha';
        $views['Paga'] = Modules::run('pedido/pedido/pedidoPago');
        echo json_encode($views);
    }
    
    public function getnombreClient($idCliente){
       $result = $this->M_cliente->getClienteID($idCliente);
       $nombre = $result->nombre.' '.$result->apellido;
       return $nombre;
    }

}
