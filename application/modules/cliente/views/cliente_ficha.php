<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div class="panel panel-default">
    <div class="panel-heading text text-center"><h3>Ficha Cliente</h3></div><div class="panel-body">
        <div class="row">
            <nav class="navbar navbar-default">
                <div class="nav nav-justified navbar-nav">
                    <form class="navbar-form navbar-search" role="search">
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-search btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="glyphicon glyphicon-search"></span>
                                    <span class="label-icon">Buscar</span>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-left" role="menu">
                                    <li>
                                        <a href="#" onclick="filtroCI()">
                                            <span class="glyphicon glyphicon-user"></span>
                                            <span class="label-icon">Buscar por C.I</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="filtroTelef()">
                                            <span class="glyphicon glyphicon-phone"></span>
                                            <span class="label-icon">Buscar por Telefono</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <input type="text" class="form-control" id="seardh">
                        </div>  
                    </form>   
                </div>
            </nav>  
            <form class="form-horizontal" role="form" id="formCli">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="name" class ="control-label col-sm-3">Nombre</label>
                        <div class="col-sm-8">
                            <input type="text" value="<?php echo $nombre = isset($ficha->nombre) ? $ficha->nombre : ''; ?>" class="form-control" id="nombre" placeholder="Nombre" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address" class ="control-label col-sm-3">Apellido</label>
                        <div class="col-sm-8">
                            <input type="text" value="<?php echo $apellido = isset($ficha->apellido) ? $ficha->apellido : ''; ?>"  class="form-control" id="apellido" placeholder="Apellido" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class ="control-label col-sm-3">C.I</label>
                        <div class="col-sm-8">
                            <input type="text" value="<?php echo $CI = isset($ficha->Ci) ? $ficha->Ci : ''; ?>" class="form-control" id="CI" placeholder="C.I" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class ="control-label col-sm-3">Genero</label>
                        <div class="col-sm-8">
                            <label class="radio-inline">
                                <input type="radio"  name="optradio" value="M" id="Genero"<?php echo $genero = isset($ficha->sexo) && $ficha->sexo == 'M' ? 'checked' : '' ?>>Masc.
                            </label>
                            <label class="radio-inline" id="Genero">
                                <input type="radio" name="optradio" value="F" id="Genero" <?php echo $genero = isset($ficha->sexo) && $ficha->sexo == 'F' ? 'checked' : '' ?>>Fem.
                            </label>
                            <label class="radio-inline" id="Genero">
                                <input type="radio" name="optradio" value="E" id="Genero" <?php echo $genero = isset($ficha->sexo) && $ficha->sexo == 'E' ? 'checked' : '' ?>>EmP.
                            </label>
                        </div>    
                    </div>
                    <div class="form-group">
                        <label for="pwd" class ="control-label col-sm-3">RUC</label>
                        <div class="col-sm-8">
                            <input type="text" value="<?php echo $RUC = isset($ficha->ruc) ? $ficha->ruc : ''; ?>" class="form-control" id="RUC" placeholder="RUC">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pwd" class ="control-label col-sm-3">telefono</label>
                        <div class="col-sm-8">
                            <input type="text" value="<?php echo $telefono = isset($ficha->telefono) ? $ficha->telefono : ''; ?>" class="form-control" id="telefono" placeholder="telefono" maxlength="11" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pwd" class ="control-label col-sm-3">telefono1</label>
                        <div class="col-sm-8">
                            <input type="text" value="<?php echo $telefono1 = isset($ficha->telefono1) ? $ficha->telefono1 : ''; ?>" class="form-control" id="telefono1" placeholder="telefono" maxlength="11">
                        </div>
                    </div>
                </div>
                <div class="col-md-4" id="Direcciones" >
                    <div class="form-group">
                        <label for="pwd" class ="control-label col-sm-3">direccion</label>
                        <div class="col-sm-8">
                            <input type="text" value="<?php echo $direccionES = isset($direccion->direccion) ? $direccion->direccion : ''; ?>" class="form-control" id="direccion" placeholder="direccion">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pwd" class ="control-label col-sm-3">calle principal</label>
                        <div class="col-sm-8">
                            <input type="text" value="<?php echo $calle1 = isset($direccion->calle1) ? $direccion->calle1 : ''; ?>" class="form-control" id="calle1" placeholder="calle Principal">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pwd" class ="control-label col-sm-3">calle inter</label>
                        <div class="col-sm-8">
                            <input type="text" value="<?php echo $calle2 = isset($direccion->calle2) ? $direccion->calle2 : ''; ?>" class="form-control" id="calle2" placeholder="calle intermediaria">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pwd" class ="control-label col-sm-3">Nro casa</label>
                        <div class="col-sm-8">
                            <input type="text" value="<?php echo $nro = isset($direccion->nro_casa) ? $direccion->nro_casa : ''; ?>" class="form-control" id="nroCasa" placeholder="nro Casa">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pwd" class ="control-label col-sm-3">Ciudad</label>
                        <div class="col-sm-8 col-sm-8">
                            <select name="state" class="form-control selectpicker" id="ciudad">
                                <option value=" " >Please select your state</option>
                                <option <?php echo $ciudad = isset($direccion->ciudad) && $direccion->ciudad == 'Fdo de Mora' ? 'selected' : '' ?> >Fdo de Mora</option>
                                <option <?php echo $ciudad = isset($direccion->ciudad) && $direccion->ciudad == 'Asuncion' ? 'selected' : '' ?> >Asuncion</option>
                                <option <?php echo $ciudad = isset($direccion->ciudad) && $direccion->ciudad == 'San. Lorenzo' ? 'selected' : '' ?>>San. Lorenzo</option>
                                <option <?php echo $ciudad = isset($direccion->ciudad) && $direccion->ciudad == 'Ñemby' ? 'selected' : '' ?>>Ñemby</option>
                                <option <?php echo $ciudad = isset($direccion->ciudad) && $direccion->ciudad == 'Lambare' ? 'selected' : '' ?>>Lambare</option>
                            </select>
                        </div>
                    </div>
<!--                    <div class="form-group">
                        <label for="pwd" class ="control-label col-sm-3">Direccion Alter</label>
                        <div class="col-sm-8">
                            <select name="state" class="form-control selectpicker" id="direcAv">
                                <option value=" " >Please select your state</option>
                                <option selected >Alabama</option>
                                <option >Arizona</option>                   
                            </select>
                        </div>
                    </div>-->
                    <?php if ($nombre): ?>
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="button" value="ir a pago pedido" class="btn btn-info"  onclick="IrPago(<?php echo $ficha->idCliente ?>, <?php echo $direccion->idDireccion ?>)">
                        </div> 
                    <?php else : ?>
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="button" value="Registra Cliente" class="btn btn-warning" onclick="ficha()">
                        </div> 
                    <?php endif; ?>
                </div>
                <div class="col-md-4">         
                    <div class="well">
                        
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="button" value="Maps" data-toggle="modal" data-target="#mapModal" class="btn btn-default" onclick="initMap()">
                        </div>
                    </div>    
                    <div class="form-group">
                        <label for="pwd" class ="control-label col-sm-3">latitud</label>
                        <div class="col-sm-8">
                            <input type="text" value="<?php echo $latitud = isset($direccion->lat) ? $direccion->lat : ''; ?>" disabled="true" class="form-control" id="latitud" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pwd" class ="control-label col-sm-3">longitud</label>
                        <div class="col-sm-8">
                            <input type="text" value="<?php echo $longitud = isset($direccion->long) ? $direccion->long : ''; ?>" disabled="true" class="form-control" id="longitud" placeholder="">
                        </div>
                    </div>
                </div>     
            </form>    
        </div>
    </div>
</div>

<!-- modal map -->
<div class="modal fade" id="mapModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Maps</h4>
            </div>
            <div class="modal-body">
                <div id="map" style="width: 100%; height:240px">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>




<!--<script type="text/javascript">
   $(".input-group-btn .dropdown-menu li a").click(function(){
       var selText = $(this).html();
       $(this).parents('.input-group-btn').find('.btn-search').html(selText);
       
   }); 
</script>-->
