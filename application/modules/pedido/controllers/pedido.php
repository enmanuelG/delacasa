<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pedido extends RH_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('M_pedido');
    }

    public function pedidoPago() {
        $data['ficha'] = $this->session->userdata('tmpFicha');
        $data['paga'] = $this->M_pedido->getPaga();
        $this->load->view('Views_pedido', $data);
    }

    public function newPedido() {
        $user = $this->session->userdata('userData');
        $idpedido = (int) $this->session->userdata('tipoPedido');
        $data['montoTotal'] = $this->session->userdata('totalCompra');
        $data['idCliente'] = $this->session->userdata('idCliente');
        $data['idDireccion'] = $this->session->userdata('idDir');
        $data['idtipoPed'] = $idpedido == 0 ? 2 : $idpedido;
        $data['idLocal'] = $user->idLocal;
        $data['idPago'] = $this->input->post('VtiPago');
        $data['pagoCont'] = $this->input->post('VpagaCon');
        $data['Cod'] = $this->M_pedido->CodOp();
        $data['fechaAlta'] = $this->M_pedido->fechaAlta();
        $idPedido = $this->M_pedido->InsPedido($data);
        if (isset($idPedido)) {
            $idCarrito = $this->carritoItems($idPedido);
            if (isset($idCarrito)) {
                $this->SessionCrack();
                $views['info'] = 'pedido registrado en cocina';
                echo json_encode($views);
            } else {
                $Views['infoError'] = 'pedido no registrado';
                echo json_encode($Views);
            }
        }
    }

    private function carritoItems($idPedido) {
        $pedido = $this->M_pedido->getPedido($idPedido);
        $tipoPedido = $this->M_pedido->tipoPtoNombre($pedido->idTipoPed);
        $data['Cliente'] = $this->session->userdata('tmpFicha');
        $data['Producto'] = $this->session->userdata('cart');
        $data['MontoTotal'] = $this->session->userdata('totalCompra');
        $data['tipoPago'] = $this->M_pedido->NamePago($pedido->Idpago);
        $data['local'] = 'Central';
        $data['TipoPedido'] = $tipoPedido;
        $data['fechaAlta'] = $pedido->fecha_alta;
        $data['idPedido'] = $pedido->idPedido;
        return $this->M_pedido->InsCarrito($data);
    }

    private function SessionCrack() {
        $array_session = array('totalCompra', 'idCliente', 'idDir', 'cart', 'tmpFicha');
        $this->session->unset_userdata($array_session);
        $this->session->set_userdata('tipoPedido', 0);
    }

    public function LisPedidos($data = null) {
        $this->load->view('list_pedidos', $data);
    }

    public function getPedidos() {
        $user = $this->session->userdata('userData');
        $desde = $this->input->post('Vdesde');
        $hasta = $this->input->post('Vhasta');
        $data['ListPedidos'] = $this->M_pedido->lisPedido(array($user->idLocal, $desde, $hasta));
        $this->LisPedidos($data);
    }

    public function getEstados() {
        $estados = $this->M_pedido->getEstado();
        return $estados;
    }

    public function ViewsTickets() {
        $idPedido = $this->input->post('idPed');
        $dataPedido = $this->M_pedido->getDatosCarrito($idPedido);
        $Views['tickets'] = $this->builTickets($dataPedido);
        echo json_encode($Views);
    }

    private function builTickets($json) {
        $data = json_decode($json);
        $cliente = $data->Cliente;
        $producto = $data->Producto;
        $tipoPed = $data->TipoPedido;
        $montoTotal = $data->MontoTotal;
        $pagoTipo = $data->tipoPago;
        $coOp = $this->M_pedido->getOp($data->idPedido);
        $strin = '<br><b>Local:</b> ' . $data->local . ', Cod.OP: <b>' . $coOp . '</b>'
                . '<br> Fecha Alta: ' . $data->fechaAlta
                . '<br>Tipo Orden: ' . $tipoPed . '<br>__________________________________'
                . '<br><b>Datos Del Cliente</b>'
                . '<br><b>Cliente:</b> ' . $cliente->nombre . '  ' . $cliente->apell
                . '<br><b>C.I:</b> ' . $cliente->CI
                . '<br><b>R.U.C:</b> ' . $cliente->ruc
                . '<br><b>Telefono1:</b> ' . $cliente->telefon
                . '<br><b>Telefono2:</b> ' . $cliente->telefon1
                . '<br><b>Ciudad:</b> ' . $cliente->ciudad
                . '<br><b>Direccion:</b> ' . $cliente->calle1 . ' c/ ' . $cliente->calle2
                . '<br>__________________________________'
                . '<br><b>Datos del Pedido</b>';
        foreach ($producto as $pro) {
            $strin .= '<br><b>Producto: </b>' . $pro->name . '<br><b>Cant: </b>' . $pro->qt;
            if ($pro->items != '') {
                $strin .= '<br><b>Detalles</b>';
                foreach ($pro->items as $tems) {
                    $strin .= '<br>&nbsp;&nbsp;&nbsp;&nbsp' . Modules::run('producto/producto/get_producto', $tems->id)->producto;
                }
            }
           //  $strin .= '<br><b>Observaciones: </b>'.$pro->descripcion ;
            $strin .= '<br><b>valor: </b>' . number_format($pro->total, 0, ',', '.') . '<br>';
        }
        $strin .= '<br>__________________________________';
        $strin .= '<br><b>Paga con: </b>' . $pagoTipo
               . '<br><b>Total a Pagar:</b> ' . number_format($montoTotal, 0, ',', '.');
        return $strin;
    }
    
    
    public function updateEstado(){
        $data['idEstado'] = $this->input->post('idEstado');
        $data['idPedido'] = $this->input->post('idPedido');
        $this->M_pedido->estados($data);
        $View['listPedido'] = Modules::run('pedido/getPedidos');
        $View['info'] = 'Estado Actualizado';
        echo json_encode($View);
    }

    public function get_estadoId($idEstado){
        $data = $this->M_pedido->getEstarows($idEstado);
        return $data;
    }
    
}
