<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div class="row" style="margin-left: 15px; margin-right: 15px" id="LisPed">
    <div class="col-md-12 well" id="central"> 
        <div class="panel panel-default">
            <div class="panel-heading text text-center">
                <h4>Listado Pedidos</h4>
            </div>
            <div class="panel-body">
                <!--            <div class="list-group" id="#">-->
                <div class="list-group">
                    <div class="list-group-item list-group-item-info"> 
                        <div class="row">
                            <div class="col-md-2">
                                <div><strong>Cod O/p</strong></div>
                            </div>
                            <div class="col-md-2">
                                <div ><strong>Fecha Pedido</strong></div>
                            </div>
                            <div class="col-md-2">
                                <div ><strong>Estado</strong></div>
                            </div>
                            <div class="col-md-3">
                                <div ><strong>Cliente</strong></div>
                            </div>
                            <div class="col-md-2">
                                <div><strong>Valor</strong></div>
                            </div>
                            <div class="col-md-1">
                                <div ><strong>Accion</strong></div>
                            </div>
                        </div>       
                    </div>
                    <?php foreach ($ListPedidos as $ped): ?>
                        <?php $estados = Modules::run('Pedido/getEstados') ?>
                        <?php $estado = Modules::run('Pedido/get_estadoId', $ped->estadoPedido) ?>
                        <?php $checke = $estado == 'Facturado' || $estado == 'Cancelado' ? 'disabled' : '' ?>
                        <?php
                        switch ($estado) {
                            case 'Facturado':
                                $color = 'list-group-item list-group-item-success';
                                break;
                            case 'Cancelado':
                                $color = 'list-group-item list-group-item-danger';
                                break;
                            default :
                                $color = 'list-group-item';
                        }
                        ?>
                        <div class="<?php echo $color ?>">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class=""><strong><?php echo $ped->CodePedido; ?></strong></div>
                                </div>
                                <div class="col-md-2">
                                    <div class=""><strong><?php echo $ped->fecha_alta; ?></strong></div>
                                </div>
                                <div class="col-md-2">
                                    <div>
                                        <select class="form-control" id="mySelect" name="seleccion"<?php echo $ped->idPedido ?> onchange="listPedEstados(<?php echo $ped->idPedido ?>, $(this).val())" <?php echo $checke; ?>>
                                            <?php foreach ($estados as $Est): ?>
                                                    <?php $select = $ped->estadoPedido == $Est->idped_estado ? 'selected' : ''; ?>
                                                <option value="<?php echo $Est->idped_estado; ?>" <?php echo $select; ?> ><?php echo $Est->estado_pedido; ?>
                                            <?php endforeach; ?>   
                                        </select>
                                    </div>    
                                </div>   
                                <div class="col-md-3">
                                    <div class=""><strong><?php echo Modules::run('cliente/cliente/getnombreClient', $ped->idCliente); ?></strong></div>
                                </div>
                                <div class="col-md-2">
                                    <div class=""><strong><?php echo number_format($ped->TotalPago, 0, ',', '.'); ?></strong></div>
                                </div>
                                <div class="col-md-1">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="#" onclick="tickets(<?php echo $ped->idPedido ?>)"><span class="glyphicon glyphicon-eye-open"></span></a> 
                                        </div>
                                        <?php if ($estado == 'Facturado') : ?>
                                            <div class="col-md-6">
                                                <a href="#"><span class="glyphicon glyphicon-print"></span></a> 
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>     
                    <?php endforeach; ?>
                </div>     
                <!--            </div> -- -->
            </div>
        </div>
    </div>     
</div>