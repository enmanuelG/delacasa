<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends RH_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('acceso/Login_model');
    }
    
    public function index() {
        $this->load->view('welcome_message');
    }
    
    public function validacionLogin(){
       $parametro = array( 'user' => $this->input->post('username'), 'pasword' => $this->Encripta($this->input->post('password')));
       $data['success']  = $this->Login_model->validacion($parametro);
       $data['credenciales'] = array();
       if(isset($data['success']) && $data['success']->estado == 1 ){ 
            $user = $data['success'];
           $this->session->set_userdata('userData',$user);
           echo 'true';
           
       } else {
           echo 'false';
       }
    }
    
    private function Encripta($password){
        $resul = sha1(md5($password));
        return $resul;
    }
    
    public function salir(){
        $this->session->sess_destroy();
        header('Location:'.base_url().'principal');
    }
}
