<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div class="row">
    <div class="col-md-1">

    </div>
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading text text-center">
                <h4>Informes</h4>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <form action="<?php echo base_url();?>reporteria/getReport" method="post"> 
                        <div class="col-md-3">
                            <label>Fecha Inicial</label>
                            <input class="form-control" type="date" required="" name="fechaIn">
                        </div> 
                        <div class="col-md-3">
                            <label>Fecha Fin</label>
                            <input class="form-control" type="date" required="" name="fechaFin">
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Tipo Informe</label>
                                <div class="input-group">
                                    <select class="form-control input-group-lg" name="tipo">
                                    <option value="1">pedidos delivery</option>
                                    <option value="2">pediddos Local</option>
                                    <option value="3">pediddos hechos</option>
                                </select>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <span class="glyphicon glyphicon-download"></span>
                                    </button>
                                </span>
                                </div>  
                            </div>
                        </div>
                       </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1">

        </div>

    </div>

