<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reporteria extends RH_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('Excel');
        $this->load->model('M_reporteria');
    }

    public function index() {
        $this->load->view('Views_reporte');
    }

    public function getReport() {
        $PHPExcel = new Excel;
        $fechaIni = $this->input->post('fechaIn');
        $fechaFin = $this->input->post('fechaFin');
        $tipo = $this->input->post('tipo');
        $data['fecha1'] = $fechaIni;
        $data['fecha2'] = $fechaFin;
        switch ($tipo) {
            case 1:
                $title = 'PedidoDelivery desde ' . $data['fecha1'] . 'hasta ' . $data['fecha2'];
                $data = $this->M_reporteria->get_PedidosDelivery($data);
                $this->buldExcelPedi($data,$PHPExcel);
                break;
            case 2:
                $title = 'PedidoLocal desde ' . $data['fecha1'] . 'hasta ' . $data['fecha2'];
                $data = $this->M_reporteria->get_PedidosLocal($data);
                $this->buldExcelPedi($data,$PHPExcel);
                break;
            case 3:
                $title = 'PedidoFull desde ' . $data['fecha1'] . 'hasta ' . $data['fecha2'];
                $data = $this->M_reporteria->get_Pedidosfull($data);
                $this->buldExcelPedi($data,$PHPExcel);
                break;
        }
        $this->GenerateExcel($title, $PHPExcel);
    }

    public function buldExcelPedi($data,$PHPExcel) {
        $PHPExcel->setActiveSheetIndex(0);
      // cabecera
        $PHPExcel->getActiveSheet()->setCellValue('A1', 'CodePedido');
        $PHPExcel->getActiveSheet()->setCellValue('B1', 'fecha_alta');
        $PHPExcel->getActiveSheet()->setCellValue('C1', 'montoPedido');
        $PHPExcel->getActiveSheet()->setCellValue('D1', 'Local');
        $PHPExcel->getActiveSheet()->setCellValue('E1', 'tipoPedido');
        $PHPExcel->getActiveSheet()->setCellValue('F1', 'pago');
        $PHPExcel->getActiveSheet()->setCellValue('G1', 'Cliente');
      // Dartos 
        $cont = 2;
        foreach ($data as $Da){
        $PHPExcel->getActiveSheet()->setCellValue('A'.$cont, $Da->CodePedido);
        $PHPExcel->getActiveSheet()->setCellValue('B'.$cont, $Da->fecha_alta);
        $PHPExcel->getActiveSheet()->setCellValue('C'.$cont, $Da->TotalPago);
        $PHPExcel->getActiveSheet()->setCellValue('D'.$cont, $Da->Local);
        $PHPExcel->getActiveSheet()->setCellValue('E'.$cont, $Da->tipoPedido);
        $PHPExcel->getActiveSheet()->setCellValue('F'.$cont, $Da->pago);
        $PHPExcel->getActiveSheet()->setCellValue('G'.$cont, $Da->nombre);
        $cont++;
        }
    }
    
    public function GenerateExcel($title, &$PHPExcel){
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel5');
        // Forzamos a la descarga
        $objWriter->save('php://output');
    }

}
