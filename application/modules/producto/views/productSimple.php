<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form id="formsimple">
    <div class="row">
        <div class="col-md-2">
            <img src="<?php echo base_url().'assets/imag/producto/'.$Producto->imagen ?>" height="60" width="60" class="img-circle">  
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <strong>Nombre: </strong><?php echo $Producto->producto; ?>
                </div>
                <div class="col-md-6">
                    <label for="#" class="text text-capitalize">Cantidad</label>
                    <input type="number" class="form-control" placeholder="cantidad" name="cantidadProduct" value="1" required>
                    <input type="hidden" name="idProducto" value="<?php echo $Producto->idproducto; ?>">
                </div>
                <div class="col-md-12">
                    <label for="#" class="text text-capitalize">Precio: </label>
                    <p><?php echo number_format($Producto->precio_delivery,0 , ',', '.')?></p>
                </div>    
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="#" class="text-capitalize">Observaciones</label>
                <textarea class="form-control" id="#" placeholder="observaciones" cols="10" rows="5" name="Observaciones"></textarea>
            </div>
        </div>
    </div>
    <div class="row-fluid text-left">
        <strong>Descripcion: </strong>
        <cite class="text-warning"><?php echo $Producto->descripcion; ?></cite>
    </div>   
</form>
