<?php
?>
<div class="container-fluid">
    <div class="well">
        <div class="card">
            <div class="panel panel-body">
            <div class="card-header">
                <h4 class="cart-title">Productos</h4>
                <div class="row">
                    <div class="col-md-8">
                        <ul class="breadcrumb">    
                            <li class="active">Categorias</li>
                            <?php foreach ($Menu as $m): ?>
                            <li onclick="get_ProductoCategoria(<?php echo $m->idCategoria ;?>)"><a href="#"><?php echo $m->categoria ?></a></li> 
                             <?php endforeach; ?>
                            <li onclick="get_ProductoCategoria('-2')"><a href="#">sabores Pizzas</a></li>                            
                        </ul>
                    </div>
                   <div class="col-md-4">
                       <a href="#" title="Agregar" onclick="ViewsReproducto()" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
                    </div>
                </div>
            </div>
                <div class="card-body" id="newProduct">
                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                        <th>Code</th>
                        <th>Producto</th>
                        <th>Descripcion</th>
                        <th>Abre</th>
                        <th>Precio</th>
                        <th>Stock</th>
                        <th>Estado</th>
                        <th>Imagen</th>
                        <th>Acciones</th>
                        </thead>
                        <tbody id="tabaConten">

                        </tbody>
                    </table>
                </div>           
            </div>
          </div>      
        </div> 
    </div>
</div>

