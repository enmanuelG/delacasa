<h2 class="text text-center">Crear Producto</h2>
<br>
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <form id="Product">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="first-name">Producto</label>
                        <input type="text" class="form-control" placeholder="producto" id="Producto" name="product">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="last-name">Descripcion</label>
                        <input type="text" class="form-control" placeholder="Descripcion" id="descripcion" name="descrip">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="country">Abre</label>
                        <input type="text" class="form-control" placeholder="abreviacion" id="abre" name="abre">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="number">Precio</label>
                        <input type="number" class="form-control" placeholder="precio producto" id="precio" name="precio">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="age">Stock</label>
                        <input type="number" class="form-control" placeholder="stock" id="stock" name="stock">
                    </div>
                </div>        
                <div class="col-md-4" >
                    <div class="form-group">
                        <label for="pizza">pizza</label>
                        <select class="form-control" id="pizza" name="pizza" disabled>
                            <option value="-1">select..Pizza</option>
                            <?php foreach ($Pizzas as $p) : ?>
                                <option value="<?php echo $p->idproducto; ?>"><?php echo $p->producto; ?></option>
                            <?php endforeach; ?>
                        </select> 
                    </div>
                </div>        
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="category">Categoria</label>
                        <select id="category" class="form-control"  name="categ">
                            <?php foreach ($Categoria as $acte) : ?>
                                <option value="<?php echo $acte->idCategoria; ?>"><?php echo $acte->categoria; ?></option>
                            <?php endforeach; ?>
                        </select> 
                    </div>    
                </div>        
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="education">Tipo</label>
                        <select onchange="validaSabor()" class="form-control" id="tipo" name="tipo">
                            <?php foreach ($tipo as $Tip) : ?>
                                <option value="<?php echo $Tip->idtipo; ?>"><?php echo $Tip->product_tipo; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>  
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Estado</label>
                        <div class="radio">
                            <label class="radio-inline">
                                <input type="radio" name="optradio" value="1">Activo</label>
                            <label class="radio-inline">
                                <input type="radio" name="optradio" value="0">Inactivo</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                </div>    
                <div class="col-md-4">
                    <div class="form-group">    
                        <button type="button" onclick="insProducto()" class="btn btn-success btn-ms btn-responsive" id="GuardarP"> <span class="glyphicon glyphicon-floppy-disk"></span> guardar</button>
                        <button type="button" onclick="load(4)" class="btn btn-danger btn-ms btn-responsive" id="cancelP"> <span class="glyphicon glyphicon-floppy-remove"></span> cancelar</button>
                    </div>   
                </div>    
            </div>
        </form>
    </div>
    <div class="col-md-2"></div>    
</div>    