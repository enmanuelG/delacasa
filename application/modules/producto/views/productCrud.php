
<?php
foreach ($producto as $p) :
    $estado = $p->activo > 0 ? 'Activo' : 'Inactivo';
    $estadoInativo = $estado == 'Inactivo' ? 'class="danger"' : '';
    ?>
    <tr <?php echo $estadoInativo ?>>
        <td><small><?php echo $p->code_producto; ?></small></td>
        <td><small><?php echo $p->producto; ?></small></td>
        <td><small><?php echo $p->descripcion; ?></small></td>
        <td><small><?php echo $p->abre; ?></small></td>
        <td><small><?php echo number_format($p->precio_delivery,0 , ',', '.'); ?></small></td>
        <td><small><?php echo $p->stock ?></small></td>
        <td><small><?php echo $estado; ?></small></td>
        <td align="center"><img src="<?php echo base_url() . 'assets/imag/producto/' . $p->imagen ?>" height="20" width="20"></td>
        <td>
            <a href="#" title="Editar" class="btn btn-warning btn-sm" data-toggle="collapse" data-target="#demo<?php echo '_' . $p->idproducto ?>" ><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
            <a href="#" title="Eliminar" onclick="DeleteProd(<?php echo $p->idproducto; ?>)" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
        </td>
    </tr>
    <tr id="demo<?php echo '_' . $p->idproducto ?>" class="collapse warning">
    <form id="prueba_<?php echo '_' . $p->idproducto ?>">  
        <td><input type="text" name="code_<?php echo $p->idproducto ?>"  class="form-control" size="6" value="<?php echo $p->code_producto; ?>"></td>
        <td><input type="text" name="producto_<?php echo $p->idproducto ?>" class="form-control" size="10" value="<?php echo $p->producto; ?>"></td>
        <td><textarea class="form-control" rows="3" cols="2" id="coment_<?php echo $p->idproducto ?>"><?php echo $p->descripcion; ?></textarea> </td>
        <td><input type="text" name="abre_<?php echo $p->idproducto ?>" class="form-control" size="6" value="<?php echo $p->abre; ?>"></td>
        <td><input type="text" name="precio_<?php echo $p->idproducto ?>" class="form-control" size="2" value="<?php echo $p->precio_delivery; ?>"></td>
        <td><input type="number" name="stock_<?php echo $p->idproducto ?>" class="form-control" min="1" max="10000" style="width: 5em;" value="<?php echo $p->stock; ?>"></td>
        <td>
            <div class="radio">
                <label><input type="radio" value="1" name="radio_<?php echo $p->idproducto; ?>" <?php echo $valor = $p->activo == 1 ? 'checked' : '' ?>>Activo</label>
            </div>
            <div class="radio">
                <label><input type="radio" value="0" name="radio_<?php echo $p->idproducto; ?>" <?php echo $valor = $p->activo == 0 ? 'checked' : '' ?>>Inactivo</label>
            </div>
        </td>
        <td></td>
        <td>
            <a href="#" title="Guardar" class="btn btn-success btn-sm" onclick="updateProducto(<?php echo $p->idproducto; ?>)"><span class="glyphicon glyphicon-floppy-saved" aria-hidden="true"></span></a>
    <!--            <a href="#" title="cargando" class="btn btn-sm"><img src="<?php echo base_url(); ?>assets/imag/cargando.gif" height="30" width="30"></a>-->
        </td>
    </form>   
    </tr>
<?php endforeach; ?>
