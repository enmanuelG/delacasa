<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_producto extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function getProducto($idCategoria){
        $this->db->select('*');
        $this->db->from('pro_producto');
        if($idCategoria != -1){
            $this->db->where('idCategoria',$idCategoria);
        }
        $this->db->where('idTipo !=', 3);
        $this->db->where('activo =', 1);
        $resul = $this->db->get();
        
        return $resul->result();
    }
    
    public function getProductoID($idProducto){
        $this->db->select('*');
        $this->db->from('pro_producto');
        $this->db->where('idproducto',$idProducto);
        $resul = $this->db->get();
        
        return $resul->row();
    }
    
    public function getSabores($idProducto){
        $this->db->select('*,(SELECT `abre` FROM `pro_producto` WHERE `idproducto` = pro_sabores.pro_sabores ) as sabores, (SELECT `descripcion` FROM `pro_producto` WHERE `idproducto` = pro_sabores.pro_sabores ) as saborDEsc', FALSE);
        $this->db->from('pro_sabores');
        $this->db->where('idproducto',$idProducto);
        $this->db->where('estado',1);
        $resul = $this->db->get();
        
        return $resul->result();
    }
    
    public function getCostoDelivery($idProducto){
        $this->db->select('*');
        $this->db->from('pro_producto');
        $this->db->where('idTipo',4);
        $resul = $this->db->get();
        
        return $resul->row();
    }
    
    public function getProductoSabores($idCategoria){
        $this->db->select('*');
        $this->db->from('pro_producto');
        $this->db->where('idTipo =', 3);
        $resul = $this->db->get(); 
        
        return $resul->result();
    }
    
    public function updProd($parametro){
        $data = array(
            'code_producto' => $parametro['cod'],
            'producto' => $parametro['Prod'],
            'descripcion' => $parametro['descrip'],
            'abre' => $parametro['abr'],
            'precio_delivery' => $parametro['prec'],
            'activo' => $parametro['est'],
            'stock' => $parametro['stock']
        );
        $this->db->where('idproducto',$parametro['idProd']);
        $this->db->update('pro_producto', $data);
    }
    
    public function CattoIdPro($idpro){
        $this->db->select('idCategoria');
        $this->db->from('pro_producto');
        $this->db->where('idproducto', $idpro);
        $result = $this->db->get();
        
        return $result->row()->idCategoria;
    }
    
    public function deletPro($id){
        $this->db->delete('pro_producto', array('idproducto' => $id));
    }
    
    public function getProductoAdmin($idCategoria){
        $this->db->select('*');
        $this->db->from('pro_producto');
        if($idCategoria != -1){
            $this->db->where('idCategoria',$idCategoria);
        }
        $this->db->where('idTipo !=', 3);
        $resul = $this->db->get();
        
        return $resul->result();
    }
    
    public function getTipoPro(){
        $this->db->select('*');
        $this->db->from('tipo_producto');
        $this->db->where('idTipo !=', 4);
        $result = $this->db->get();
        
        return $result->result();
    }
    
    public function getCompuestoP(){
        $this->db->select('*');
        $this->db->from('pro_producto');
        $this->db->where('idTipo ', 2);
        $result = $this->db->get();
        
        return $result->result();
    }
    
    public function insProduct($p){
        $data = array('code_producto' => '', 'producto' => $p['product'], 'descripcion' => $p['descrip'], 'abre' => $p['abre'],
                      'precio_delivery' => $p['precio'], 'precio_carry' => $p['precio'], 'activo' => $p['optradio'], 'stock' => $p['stock'], 
                      'imagen' => 'inconicto.jpg', 'idCategoria' => $p['categ'], 'idTipo' => $p['tipo'], 'CantMax' => null);
        
        $this->db->insert('pro_producto', $data);
        return $this->db->insert_id();
    }
    
    public function isnSabor($s){
        $data  =  array('estado' => '1', 'precio' => '0', 'pro_sabores' => $s['idSabor'], 'idproducto' => $s['idPizza']);
        $this->db->insert('pro_sabores',$data);
    }

}
