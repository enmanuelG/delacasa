<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 * @property CI_Benchmark $benchmark
 * @property CI_Calendar $calendar
 * @property CI_Cart $cart
 * @property CI_Config $config
 * @property CI_Controller $controller
 * @property CI_Email $email
 * @property CI_Encrypt $encrypt
 * @property CI_Exceptions $exceptions
 * @property CI_Form_validation $form_validation
 * @property CI_Ftp $ftp
 * @property CI_Hooks $hooks
 * @property CI_Image_lib $image_lib
 * @property CI_Input $input
 * @property CI_Language $language
 * @property CI_Loader $load
 * @property CI_Log $log
 * @property CI_Model $model
 * @property CI_Output $output
 * @property CI_Pagination $pagination
 * @property CI_Parser $parser
 * @property CI_Profiler $profiler
 * @property CI_Router $router
 * @property CI_Session $session
 * @property CI_Sha1 $sha1
 * @property CI_Table $table
 * @property CI_Trackback $trackback
 * @property CI_Typography $typography
 * @property CI_Unit_test $unit_test
 * @property CI_Upload $upload
 * @property CI_URI $uri
 * @property CI_User_agent $user_agent
 * @property CI_Validation $validation
 * @property CI_Xmlrpc $xmlrpc
 * @property CI_Xmlrpcs $xmlrpcs
 * @property CI_Zip $zip
 */
class RH_Controller extends MX_Controller {

//    public $layout_view = 'layout/default';
    protected $requestController;
    protected $requestAction;
    protected $session_data;

    function __construct() {
        parent::__construct();
        $this->requestController = strtolower($this->router->fetch_class());
        $this->requestAction = strtolower($this->router->fetch_method());

        $this->session_data = $this->session->userdata('userData');
        $this->token = $this->session->userdata('access_token');
        //$this->checkLogin();
        //Variables utilizadas en todas las vistas.
        $this->load->vars(array("user_name" => $this->session->userdata('user'), 'user_data' => $this->session_data, 'action' => $this->requestAction, 'controller' => $this->requestController));
    }

    protected function checkLogin() {
        if (in_array($this->requestController, ['rhskytel', 'login']) && in_array($this->requestAction, ['inicio', 'autenticar', 'index'])) {
            return TRUE;
        }
//        if (!$this->token) {
        if (!$this->session->userdata('userData')) {
            //If no session, redirect to login page
            if ($this->input->is_ajax_request()) {
                exit('logout');
            }
            redirect(base_url('/login'), 'refresh');
        }
    }

    public function _remap($method, $param) {
        if (method_exists($this, $method)) {
            $this->$method($param);
        } else {
            echo 'Error. No se encuentra el recurso solicitado';
        }
    }

    /* ---------------------------------------------------------------------------- */

    public function _result($response, $status) {
        $response['success'] = $status;
        echo json_encode($response);
    }

    public function builtExcel($params) {
        $this->load->library('PhpExcel');
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $params . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    public function setExcelProperties() {
        // Set properties
        $this->objPHPExcel->getProperties()->setCreator("RHSkytel");
        $this->objPHPExcel->getProperties()->setLastModifiedBy("RHSkytel");
        $this->objPHPExcel->getProperties()->setTitle("Office XLS Document");
        $this->objPHPExcel->getProperties()->setSubject("Office XLS Document");
        $this->objPHPExcel->getProperties()->setDescription("Archivo Excel");
    }

}
