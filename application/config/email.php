<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * EMAIL Configuration...
 */

$config['protocol'] = PROTOCOL;
$config['smtp_host'] = SMTP_HOST;
$config['smtp_user'] = SMTP_USER;
$config['smtp_pass'] = SMTP_PASS;
$config['mailtype'] = 'html';
$config['charset'] = 'iso-8859-1';