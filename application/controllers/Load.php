<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Load extends RH_Controller{

    public function __construct()    {
        parent::__construct();
        $this->load->model('m_general');
    }

    public function index() {
        if ($this->session->userdata('userData')) {
          $Module = $this->input->post('module');
          
          switch ($Module) {
            case 1:
                $primaryHtml = Modules::run('Templates/menu');
                // $secundario = Modules::run('Carrito/Compras');
                break;
            case 2:
                $primaryHtml = Modules::run('pedido/getPedidos');
                break;
            case 3:
                $primaryHtml = Modules::run('reporteria/index');
                break;
            case 4:
                $primaryHtml = Modules::run('producto/admin');
                break;
            }
             $data = ['primario' => $primaryHtml];
             echo json_encode($data);
        }
       
    }

}