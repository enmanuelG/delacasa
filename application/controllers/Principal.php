<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends RH_Controller{

    public function __construct() {
        parent::__construct();
        $this->load->model('m_general');
    }

    public function index() {
        if ($this->session->userdata('userData')) {  
            $this->session->set_userdata('tipoPedido', 0);
          $this->load->view('principal');
        } else {
            $this->load->view('acceso/login');
        }
    }

}
